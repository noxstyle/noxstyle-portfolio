<?php
/**
 * Simple script to mail the form data forward...
 * Takes in only AJAX requests...
 *
 * Note: 'e75' is used as the email field, therefore 'email' is a hidden field which should not be filled.
 * If 'email' is present we can safely reject the form.
 */

const DESTINATION_ADDRESS = 'joni.lepisto@noxstyle.info';
const SENDER_ADDRESS = 'info@noxstyle.info';
const MESSAGE_TITLE = 'Contact request from Portfolio';

$requiredFields = array(
    'e75' => array(
        'name' => 'Email',
        'onEmpty' => "Oh snap! I didn't recieve your email address. Could you please type it in and re-submit?",
        'onInvalid' => "Whoa! That doesn't look like an valid email address, are you sure you didn't misspell?",
    ),
    'name' => array(
        'name' => 'Name',
        'onEmpty' => "Pardon, I didn't catch your name. Could you repeat it please?",
    ),
    'message' => array(
        'name' => 'Message',
        'onEmpty' => 'Everyone likes messages... So leave me one, will ya?',
    ),
);

# Check for xmlhttprequest
if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
{
    # Spam protection
    if (isset($_POST['email']) && !empty($_POST['email']))
    {
        echo json_encode(array(
            'status' => 'fail',
            'data' => array('email' => 'Unacceptable data passed.'),
        ));
        exit;
    }

    # Map passed in data to message
    $message = '';
    foreach ($requiredFields as $field => $d)
    {
        if (empty($_POST[$field]))
            $errors[$field] = $d['onEmpty'];
        else
            $message .= "{$d['name']}\n------------\n".$_POST[$field]."\n\n";
    }

    $data = null;
    if (isset($errors)) {
        foreach ($errors as $field=>$error)
            $data[$field] = $error; 
    }

    # Validate Email
    if (!empty($_POST['e75']))
    {
        if (!filter_var($_POST['e75'], FILTER_VALIDATE_EMAIL))
            $data['e75'] = $requiredFields['e75']['onInvalid'];
    }

    # Mail the form if there are no errors
    if (is_null($data))
        $ms=mail(DESTINATION_ADDRESS, MESSAGE_TITLE, $message, 'From: '.SENDER_ADDRESS);

    echo json_encode(array(
        'status' => is_null($data) ? 'success' : 'fail',
        'data' => $data,
        'mailstatus' => $ms
    ));

    exit;
}

header("HTTP/1.0 404 Not Found");