/*
 * Portfolio scripts...
 */
 (function() {
    "use strict";

    $(document).ready(function(){

        // Check window state - If portfolioproject is requested - launch it..
        var currentHash = window.location.hash;
        if (currentHash.indexOf('#portfolio#') !== -1) {
            loadPortfolioContent(currentHash.replace('#portfolio#',''));
        };

        // Test for mobile
        $.isMobile = ( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) ? true : false;

        // Set lazyload
        $('#portfolio img.lazy').lazyload();

        // Set placeholder
        $('input,textarea').placeholder();

        // Portfolio opacity hover.
        if (!$.isMobile) {
            $('#portfolio .media-holder a').on('mouseover',function(){
                $(this).find('.opacity').stop(true).animate({
                    'opacity': .3
                }, 200);
            }).on('mouseout',function(){
                $(this).find('.opacity').stop(true).animate({
                    'opacity': 1
                }, 200);
            });
        }

        // Set portfolio tags
        var portfolioTags = [];
        $.each($('#portfolio .entries > li'), function(i,v){
            var tags = $(v).attr('data-tags');
            if (tags) {
                tags = tags.split(',');
                portfolioTags = portfolioTags.concat(tags);
            }
        });

        // Get unique tag values
        portfolioTags = $.grep(portfolioTags, function(v, k){
            return $.inArray(v, portfolioTags) === k;
        });

        // Map data to selector
        $.each(portfolioTags, function(i,v){
            $('#pf-tag-selector > ul').append('<li data-target="'+v+'" class="btn small dark">'+v+'</li>');
        });

        // Selection events
        $('#pf-tag-selector li').on('click',function(){
            // Toggle active state
            $(this).parent().find('li').removeClass('active').end().end().addClass('active');

            // Hide & show elements
            $('#portfolio ul.entries li')
                .hide()
                .filter('[data-tags*="'+$(this).attr('data-target')+'"]')
                .show();
        });


        // Mobile & Small screens
        $(window).on('scroll', function(){
            if ($(document).scrollTop() > 40) {
                if (!$('#nav').hasClass('on-top'))
                    $('#nav').addClass('on-top');
            } else {
                if ($('#nav').hasClass('on-top'))
                    $('#nav').removeClass('on-top');
            }
        });

        // Hook overlay event
        $('#portfolio .media-holder a').on('click', function(e){
            e.preventDefault();
            $.curScrollPos = $(document).scrollTop();

            loadPortfolioContent($(this).attr('portfolio-data'));
        });

        /* Contact form submit
         * =================== */
        $('#contact-form').on('submit', function(e){

            $('input,textarea', '#contact-form').removeClass('error');
            $('#contact-status').html('').hide();
            var status = $('#contact-status');


            $.ajax({
                type:'post',
                dataType:'json',
                url: 'lib/mail_form.php',
                data: $(this).serialize(),
                success:function(d){
                    if (d.status == 'success') {
                        $('#contact-form')
                            .slideUp()
                            .next()
                            .slideDown();
                    } else {
                        status.append('<h5>Thank you for contacting, but... There were some errors:</h5><ul></ul>');
                        $.each(d.data, function(i,v){
                            $('#contact-form [name='+i+']').addClass('error');
                            status.find('ul').append('<li>'+v+'</li>');
                        });
                        status.show();
                    }
                },
                error: function() {
                    $('#contact-status').html('<h5>Aw, Snap!</h5><p>Something went wrong! =/</p>').show(200);
                }
            });

            // Exxxxxxecution mustttt stoppp!
            return false;
        });

        /* Scrollspy
        * ================= */
        $('#nav').scrollSpy('enable');
    });

    /*
     * Initializes navigation scrollspy
     */
    $.fn.scrollSpy = function( status ) {

        if (status == 'enable') {
            var lastId,
                topMenu = $(this),
                topMenuHeight = topMenu.outerHeight()+15,
                
                // All list items
                menuItems = topMenu.find("a"),
            
                // Anchors corresponding to menu items
                scrollItems = menuItems.map(function(){
                    var item = $($(this).attr("href"));
                    if (item.length) { return item; }
                });

            // Bind click handler to menu items
            // so we can get a fancy scroll animation
            menuItems.click(function(e){
                e.preventDefault();

                // Grab current has for updating location
                var hash = this.hash;

                // Determine offset where to scroll
                var href = $(this).attr("href"),
                offsetTop = href === "#profile" ? 0 : $(href).offset().top;

                // Scroll It!
                $('html, body').stop().animate({ 
                    scrollTop: offsetTop
                }, 300, function() { window.location.replace(hash)  });
            });

            // Bind to scroll
            $(window).scroll(function(){
                // Get container scroll position
                var fromTop = $(this).scrollTop()+topMenuHeight;

                // Get id of current scroll item
                var cur = scrollItems.map(function(){
                    if ($(this).offset().top < fromTop)
                        return this;
                });
                
                // Get the id of the current element
                cur = cur[cur.length-1];
                var id = cur && cur.length ? cur[0].id : "";

                if (lastId !== id) {
                    lastId = id;
            
                    // Set/remove active class
                    menuItems.removeClass('active');
                    menuItems.filter("[href=#"+id+"]").addClass("active");

                    // Update hash - removing id attr prevents browser's default scrolling
                    var el = $('#'+lastId).removeAttr('id');
                    //window.location.hash = '#'+lastId;
                    window.location.replace('#'+lastId);
                    el.attr('id', lastId);
                }
            });

        } else {
            $(window).off('scroll');
            $('#scroll-up').off('click');
        }
    };

    var loadPortfolioContent = function(project) {
        // If project is passed as hash (#project) - the request will bring up the directory
        // root of the data folder - thus eliminate any hashtags found...
        if (project.indexOf('\#') !== -1)
            project = project.replace('#','');

        // Load portfolio data
        $.ajax({
            url:'data/portfolio/'+project+'.html',
            success:function(d){
                $('body').addClass('overlay');
                $('#portfolio-overlay-inner').html(d);

                // Init scripts
                $('#portfolio-overlay').show().overlayEvents('enabled');
                $("#portfolio-overlay .portfolio-images").responsiveSlides({
                    pager:true,
                    speed: 800,
                    timeout: 3000
                });
                
                // Disable scrollspy and replace window hash
                $('#nav').scrollSpy('disable');
                window.location.replace('#portfolio#'+project);

                /* Scroll to beginning of the portfolio entry
                 * (in case the entry does not fit to the screen) */
                $('html,body').scrollTop(0);
            },
            error: function() {
                $('#portfolio div.in').prepend('<div class="error">Requested portfolio entry not found.</div>');
            }
        });
    };

    $.fn.overlayEvents = function( status ) {
        var el = this;

        if (status == 'enabled') {
            $('a.close', this).on('click.closeOverlay', function(e) {
                e.preventDefault();
                closeOverlay();
            });

            $(document).on('keyup.closeOverlay', function(e){
                if (e.keyCode == 27)
                    closeOverlay();
            });

        } else {
            // remove the events
            el.off('click.closeOverlay');
            $(document).off('keyup.closeOverlay');
        }

        var closeOverlay = function() {
            el.hide().overlayEvents('disable');
            $('body').removeClass('overlay');

            $('html,body').scrollTop($.curScrollPos);
            $('#nav').scrollSpy('enable');
        }
    };
}(jQuery));